---
sidebar_position: 1
title: Introduction
slug: /
---

# Dev Journey

This repo's goal is to document the journey into the development world and to serve as kindaof a guide to newcomers and those who are looking for references.

Every help are welcome!

## URL's

The following URL's provide access to the present website and it's repository:

  - [devjourney.osouza.de](http://devjourney.osouza.de/)
  - [repository url](https://gitlab.com/osouza.de/dev-journey/)

## PS: It's a eternal Work In Progress ;)
