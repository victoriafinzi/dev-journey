---
sidebar_position: 2
id: ssh_key
title: SSH Key
slug: ssh/key
tags:
    - ssh
    - encryption
---
# SSH Key

## Generating

1. Generate the key

```
ssh-keygen -t ed25519
```
Something like this will be returned

```
Generating public/private ed25519 key pair.

Enter file in which to save the key (/home/user/.ssh/id_ed25519):
```

Set a custom name for the key or just press enter to use default.

2. Get the Public SSH Key

Open the .pub part of the just created SSH Key (note the file name you choose...)

```
cat /home/user/.ssh/id_ed25519.pub
```

If you're using windows, go to your user home > .git and get the .pub file


## Key Agent

1. Add the following piece of code to your shell init script (for example: ~/.bashrc)

```
if ! pgrep -u "$USER" ssh-agent > /dev/null; then
    ssh-agent -t 1h > "$XDG_RUNTIME_DIR/ssh-agent.env"
fi
if [[ ! "$SSH_AUTH_SOCK" ]]; then
    source "$XDG_RUNTIME_DIR/ssh-agent.env" >/dev/null
fi
```

2. Now, set the SSH Agent  at  ~/.ssh/config

```
AddKeysToAgent yes
```


## Refs

  - [GitLab SSH article](https://docs.gitlab.com/ee/ssh/index.html)
  - [ArchWiki - SSHKeys](https://wiki.archlinux.org/title/SSH_keys#SSH_agents)
