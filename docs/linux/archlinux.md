---
sidebar_position: 1
id: archlinux
title: Archlinux
slug: arch
tags:
    - linux
---

# Arch Linux (Manjaro, please)

## Default shell

Manjaro has a nice shell, make use of it:

```
sudo chsh -s /bin/zsh $USER
```

## Base packages and AUR

0 - Configure pacman to use simultaneous downloads :)

Edit `/etc/pacman.conf` line "ParallelDownloads" to something like 5
```
sudo sed -i -e "s/.*ParallelDownloads.*/ParallelDownloads = 5/" /etc/pacman.conf
```

1 - First, make sure to update your system

    pacman -Syyu

2 - Then, install the basic packages

    pacman -S base-devel docker docker-compose git terminator vscode

3 - Now, let's configure "yay" package manager to install AUR packages
```
git clone https://aur.archlinux.org/yay-git.git
cd yay-git
makepkg -si
```

4 - Install some usefull packages from AUR

    yay -S google-chrome teamviewer


## Flutter
```
# Configure Snap
sudo ln -s /var/lib/snapd/snap /snap

# Install flutter via snap
sudo snap install flutter --classic

# Install android studio and sdk stuff
yay -S android-studio android-sdk-cmdline-tools-latest

# Then, run flutter-doctor to check
flutter doctor
```

If you installed `google-chrome-stable`, there'll be a "GOOGLE CHROME not found error". Fix it:

```
sudo ln -s /usr/bin/google-chrome-stable /usr/bin/google-chrome
``` 

## Docker

If you followed the [guide above](#base-packages-and-aur), you'll already have Docker installed in your system.

But now you should configure it properly to autostart on every boot.

```
# Enable the docker service to start with the system
sudo systemctl enable docker

# Do activate docker now, so you can use it without reboot
sudo systemctl start docker
```