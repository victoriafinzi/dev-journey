---
sidebar_position: 1
id: gitpod
slug: gitpod
title: Gitpod
tags:
    - environments
---
# Gitpod

Everything related to my cloud-based dev enviroment.

## Docker imaging article
  - [Here](https://www.gitpod.io/docs/config-docker) explains a lil bit of how can we set a custom docker image for our Gitpod workspace.
  - [This article](https://www.gitpod.io/blog/gitpodify/#getting-a-virtual-desktop) shows how can we have a fully working remote Desktop running on a Gitpod instance.
  - [All That Developers Need Is a Browser](https://codefresh.io/howtos/developers-need-browser-productive-less/)


## GPG Signing

I like to have my commits with the "Verified" status on github and gitlab ;)

To do so, we need to generate a GPGKey, add to your git provider set GitPod to use it.

1. Follow [this guide](./encryption/gpg_key.md) to get your GPG Key.

2. Add the private GPGKey (result from $GPGKEY_PK) to Gitpod as a environment var

Head to [this url](https://gitpod.io/variables) and add a new env var with the parameters:

    - Name: GPGKEY
    - Value: <GPGKEY>
    - Scope: */*

3 - Create a dotfile repository with the [setup.sh](https://gitlab.com/osouza_de/utils/dotfiles/-/blob/main/setup.sh) and set gitpod to use it

  [Here](https://gitpod.io/preferences) you should set your dotfiles repository address.


## Refs
  - [Setup.sh dotfile](https://gitlab.com/osouza_de/utils/dotfiles/-/blob/main/setup.sh)
  - [GitLab article on how to GPG commit sign](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/index.html)
  - [Adangel article 'bout gitpod gpg git](https://adangel.org/2021/11/07/gitpod-gpg-signed-commits/)
  - [Gitpod dotfiles](https://www.gitpod.io/docs/config-dotfiles)


//TODO improve
