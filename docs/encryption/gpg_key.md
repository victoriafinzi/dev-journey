---
sidebar_position: 1
id: gpg_key
title: GPG Key
slug: gpg/key
tags:
    - gpg
    - encryption
---
# GPG Key

## Generating

1 - Generate a GPGKey, following the steps given by the command outputs.

    gpg --full-gen-key

2 - Export the GPGKey Fingerprint, Private and Public keys

    # List the keys with:
    gpg --list-secret-keys --keyid-format LONG

    # Or simply
    gpg -aK

    # Get the fingerprint
    export GPGKEY_FP=$(gpg --list-secret-keys --with-colons --fingerprint | awk -F: '$1 == "fpr" {print $10;}' | head -1)
    
    # Get the full private key
    export GPGKEY_PK=$(gpg --export-secret-keys ${GPGKEY_FP} | base64 -w 0)

    # Get the public key
    export GPGKEY_PB=$(gpg --armor --export ${GPGKEY_FP})


## Encrypting

While working at some corporative projects, you'll probably handle CONFIDENTIAL INFORMATION.

Therefore, be sure to properly protect it.

```
gpg -aesr dennis@cloudopss.com.br -o <file.asc> <file.txt>

# It's a shortcut of:
# gpg --armor --encrypt --sign --recipient dennis@cloudopss.com.br --output <file.asc> <file.txt>
```


### Symmetric
You can encrypt a file using a symmetric method `gpg -c`. But its not safe.

If you want to use it anyway, do in a safer approach

```
gpg -c --s2k-cipher-algo AES256 --s2k-digest-algo SHA512 --s2k-count 65536 <file>
```


## Decrypting

// TODO

## Edit GPG Encrypted files on-the-fly
1 - Install the tool (vim-gnupg)
```
yay -S vim-gnupg
# This way, you can edit gpg encrypted files with vim
vim <file.asc>
```


## Checking 

// TODO


## Key Agent

// TODO

Maybe [here](https://wiki.archlinux.org/title/GnuPG#gpg-agent) we can see something...


## Refs

- [ArchWiki - GnuPG](https://wiki.archlinux.org/title/GnuPG#Encrypt_and_decrypt)

