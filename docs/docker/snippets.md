---
sidebar_position: 1
id: snippets
title: Snippets
tags:
    - docker
    - basic
---
# Docker Snippets

## Alpine

This'll create a alpine linux container and install the basic tools
```

docker run -v "${PWD}:/test:rw" -it --rm --name alpine alpine sh

apk add curl git gpg ssh vim
```

## Python

```
docker run -it --rm -v "${PWD}:/app" -p 8080:8000 python bash
```


## React


### Creating a new project
The following piece of code generates a basic React application

```
docker run -v "${PWD}:/app:rw" --workdir /app --rm -it node:14 bash

npx create-react-app bahlinktri
```

### Running a dev project



## Docusaurus

### Creating a new project

[Project URL](https://docusaurus.io/docs)

```
## Create the project directory and get into it
mkdir <project_name>

## Start a Docker container
docker run --user node -p 3000:3000 -v "${PWD}:/app:rw" --workdir /app --rm -it node:17 bash

### Inside the container, create a Docusaurus App
npx -y create-docusaurus@latest <project_name> classic --package-manager npm


## Then, exit the container, cd into the project dir, run docker and start the App
cd <project_name>
docker run --user node -p 3000:3000 -v "${PWD}:/app:rw" --workdir /app --rm -it node:17 bash
npx docusaurus start -h 0.0.0.0

```

https://docusaurus.io/docs/next/docs-introduction#docs-only-mode



https://react-icons.github.io/react-icons/icons?name=fa