// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Dev Journey',
  tagline: 'A journey into the I.T. Development World',
  url: 'https://devjourney.osouza.de',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'osouza.de', // Usually your GitHub org/user name.
  projectName: 'dev-journey', // Usually your repo name.

  i18n: {
    defaultLocale: 'en',
    locales: ['en', 'pt', 'de']
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          // routeBasePath: '/',
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl: 'https://gitlab.com/osouza.de/dev-journey/-/tree/main',
        },
        blog:
        {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      hideableSidebar: false,
      autoCollapseSidebarCategories: false,
      colorMode: {
        defaultMode: 'dark',
        disableSwitch: false,
        respectPrefersColorScheme: false,
      },
      navbar: {
        title: 'Dev Journey',
        // logo: {
        //   alt: 'Dev Journey logo',
        //   src: 'img/logo.svg',
        // },
        items: [
          {
            type: 'localeDropdown',
            position: 'right',
          },
          {            
            type: 'doc',
            docId: 'intro',
            position: 'left',
            label: 'Papers',
          },
          {to: '/blog', label: 'Blog', position: 'left'},
        ],
      },
      footer: {
        style: 'dark',
        links: [ ],
        copyright: `Copyleft ${new Date().getFullYear()} osouza.de Dev Journey. Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
